﻿namespace SaladDesktop
{
    partial class AddEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.costTextBox = new System.Windows.Forms.TextBox();
            this.caloriesTextBox = new System.Windows.Forms.TextBox();
            this.weightTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.countOfSeedsTextBox = new System.Windows.Forms.TextBox();
            this.countryTextBox = new System.Windows.Forms.TextBox();
            this.sweetTextBox = new System.Windows.Forms.TextBox();
            this.timeCollectionTextBox = new System.Windows.Forms.TextBox();
            this.timeBoiledTextBox = new System.Windows.Forms.TextBox();
            this.levelVitaminCTextBox = new System.Windows.Forms.TextBox();
            this.sizeBoneTextBox = new System.Windows.Forms.TextBox();
            this.editButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(12, 358);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "Ок";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(242, 358);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // typeComboBox
            // 
            this.typeComboBox.FormattingEnabled = true;
            this.typeComboBox.Items.AddRange(new object[] {
            "BoiledVegetable",
            "FreshVegetable",
            "Citrus",
            "Drupaceous"});
            this.typeComboBox.Location = new System.Drawing.Point(134, 12);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(121, 21);
            this.typeComboBox.TabIndex = 5;
            this.typeComboBox.SelectedIndexChanged += new System.EventHandler(this.typeComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Имя:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Цена:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Калории:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Вес:";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(134, 58);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(100, 20);
            this.nameTextBox.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Тип:";
            // 
            // costTextBox
            // 
            this.costTextBox.Location = new System.Drawing.Point(134, 84);
            this.costTextBox.Name = "costTextBox";
            this.costTextBox.Size = new System.Drawing.Size(100, 20);
            this.costTextBox.TabIndex = 12;
            this.costTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.costTextBox_KeyPress);
            // 
            // caloriesTextBox
            // 
            this.caloriesTextBox.Location = new System.Drawing.Point(134, 110);
            this.caloriesTextBox.Name = "caloriesTextBox";
            this.caloriesTextBox.Size = new System.Drawing.Size(100, 20);
            this.caloriesTextBox.TabIndex = 13;
            this.caloriesTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.caloriesTextBox_KeyPress);
            // 
            // weightTextBox
            // 
            this.weightTextBox.Location = new System.Drawing.Point(134, 137);
            this.weightTextBox.Name = "weightTextBox";
            this.weightTextBox.Size = new System.Drawing.Size(100, 20);
            this.weightTextBox.TabIndex = 14;
            this.weightTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.weightTextBox_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 163);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Количество семян:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Страна:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 217);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Сладость:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 244);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Время сбора:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 271);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Время варки:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 298);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(116, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Уровень витамина С:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 325);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "Размер косточки:";
            // 
            // countOfSeedsTextBox
            // 
            this.countOfSeedsTextBox.Location = new System.Drawing.Point(134, 164);
            this.countOfSeedsTextBox.Name = "countOfSeedsTextBox";
            this.countOfSeedsTextBox.Size = new System.Drawing.Size(100, 20);
            this.countOfSeedsTextBox.TabIndex = 22;
            this.countOfSeedsTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.countOfSeedsTextBox_KeyPress);
            // 
            // countryTextBox
            // 
            this.countryTextBox.Location = new System.Drawing.Point(134, 191);
            this.countryTextBox.Name = "countryTextBox";
            this.countryTextBox.Size = new System.Drawing.Size(100, 20);
            this.countryTextBox.TabIndex = 23;
            // 
            // sweetTextBox
            // 
            this.sweetTextBox.Location = new System.Drawing.Point(134, 217);
            this.sweetTextBox.Name = "sweetTextBox";
            this.sweetTextBox.Size = new System.Drawing.Size(100, 20);
            this.sweetTextBox.TabIndex = 24;
            // 
            // timeCollectionTextBox
            // 
            this.timeCollectionTextBox.Location = new System.Drawing.Point(134, 245);
            this.timeCollectionTextBox.Name = "timeCollectionTextBox";
            this.timeCollectionTextBox.Size = new System.Drawing.Size(100, 20);
            this.timeCollectionTextBox.TabIndex = 25;
            this.timeCollectionTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.timeCollectionTextBox_KeyPress);
            // 
            // timeBoiledTextBox
            // 
            this.timeBoiledTextBox.Location = new System.Drawing.Point(134, 271);
            this.timeBoiledTextBox.Name = "timeBoiledTextBox";
            this.timeBoiledTextBox.Size = new System.Drawing.Size(100, 20);
            this.timeBoiledTextBox.TabIndex = 26;
            this.timeBoiledTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.timeBoiledTextBox_KeyPress);
            // 
            // levelVitaminCTextBox
            // 
            this.levelVitaminCTextBox.Location = new System.Drawing.Point(134, 298);
            this.levelVitaminCTextBox.Name = "levelVitaminCTextBox";
            this.levelVitaminCTextBox.Size = new System.Drawing.Size(100, 20);
            this.levelVitaminCTextBox.TabIndex = 27;
            this.levelVitaminCTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.levelVitaminCTextBox_KeyPress);
            // 
            // sizeBoneTextBox
            // 
            this.sizeBoneTextBox.Location = new System.Drawing.Point(134, 324);
            this.sizeBoneTextBox.Name = "sizeBoneTextBox";
            this.sizeBoneTextBox.Size = new System.Drawing.Size(100, 20);
            this.sizeBoneTextBox.TabIndex = 28;
            this.sizeBoneTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sizeBoneTextBox_KeyPress);
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(120, 358);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(92, 23);
            this.editButton.TabIndex = 29;
            this.editButton.Text = "Редактировать";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // AddEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 400);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.sizeBoneTextBox);
            this.Controls.Add(this.levelVitaminCTextBox);
            this.Controls.Add(this.timeBoiledTextBox);
            this.Controls.Add(this.timeCollectionTextBox);
            this.Controls.Add(this.sweetTextBox);
            this.Controls.Add(this.countryTextBox);
            this.Controls.Add(this.countOfSeedsTextBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.weightTextBox);
            this.Controls.Add(this.caloriesTextBox);
            this.Controls.Add(this.costTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.typeComboBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Name = "AddEditForm";
            this.Text = "AddEditForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ComboBox typeComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox costTextBox;
        private System.Windows.Forms.TextBox caloriesTextBox;
        private System.Windows.Forms.TextBox weightTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox countOfSeedsTextBox;
        private System.Windows.Forms.TextBox countryTextBox;
        private System.Windows.Forms.TextBox sweetTextBox;
        private System.Windows.Forms.TextBox timeCollectionTextBox;
        private System.Windows.Forms.TextBox timeBoiledTextBox;
        private System.Windows.Forms.TextBox levelVitaminCTextBox;
        private System.Windows.Forms.TextBox sizeBoneTextBox;
        private System.Windows.Forms.Button editButton;
    }
}