﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logic;
using Entities;

namespace SaladDesktop
{
    public partial class MainForm : Form
    {
        Salad salad1 = IngredientFactory.CreateSalad();
        //List<AbstractIngredient> ingredientsList;
        public MainForm()
        {
            InitializeComponent();
        }
        
        private void addButton_Click(object sender, EventArgs e)
        {

            AddEditForm addForm = new AddEditForm();
            addForm.ShowDialog();
            //ingredientsListBox.DataSource = null;
           if (addForm.Result == DialogResult.OK)
           {
               
               salad1.AddIngredient(addForm.Ingredient);
               UpdateData();
               UpdateLabels();
           }
            this.Refresh();
            UpdateLabels();
           
        }
        public void UpdateLabels()
        {
            weightLabel.Text = String.Format("Weight = {0}", Calculator.GetTotalWeight(salad1).ToString());
            caloriesLabel.Text = String.Format("Calories = {0}", Calculator.GetTotalCalories(salad1).ToString());
        }
        private void MainForm_Load(object sendser, EventArgs e)
        {

            UpdateData();
            //ingredientsListBox.DisplayMember = "Name";
            UpdateLabels();
        }
        void UpdateData()
        {
            ingredientsListBox.DataSource = null;
            ingredientsListBox.DataSource = salad1.Ingredient; //ingredientsList;
            ingredientsListBox.DisplayMember = "Name";
            this.Refresh();
        }
        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            AddEditForm form = new AddEditForm((AbstractIngredient)ingredientsListBox.SelectedItem);
            form.ShowDialog();
            UpdateData();
            this.Refresh();
            UpdateLabels();
            //ingredientsListBox.DataSource = null;
            //ingredientsListBox.DisplayMember = "Name";
        }
        
    }
}
