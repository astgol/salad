﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entities;
using Logic;

namespace SaladDesktop
{
    public partial class AddEditForm : Form
    {

        string _type = null;
        AbstractIngredient _ingredient = null;
        //Salad _salad = null;
        public AddEditForm()
        {
            InitializeComponent();
            editButton.Enabled = false;
        }
        public AddEditForm(AbstractIngredient ingredient)
        {
            InitializeComponent();
            Ingredient = ingredient;
            string type;
            type = Convert.ToString(ingredient.GetType());
            string[] split = type.Split('.');
            type = split[1];
            _type = type;
            FillFields(type);
            typeComboBox.Enabled = false;
            okButton.Enabled = false;
            
        }
        public AbstractIngredient Ingredient { get; set; } //Автосвойство
        public void FillFields(string type)
        {
           
            DisableFields(type);
           
            switch (type)
            {
                case "Citrus":
                    {
                        Citrus citrus = (Citrus)Ingredient;
                        nameTextBox.Text = citrus.Name;
                        costTextBox.Text = citrus.Cost.ToString();
                        caloriesTextBox.Text = citrus.Calories.ToString();
                        weightTextBox.Text = citrus.Weight.ToString();
                        levelVitaminCTextBox.Text = citrus.LevelVitaminC.ToString();
                        countryTextBox.Text = citrus.Country;
                        sweetTextBox.Text = citrus.Sweet;
                    }break;
                case "BoiledVegetable":
                    {
                        BoiledVegetable boiledVegetable = (BoiledVegetable)Ingredient;
                        nameTextBox.Text = boiledVegetable.Name;
                        costTextBox.Text = boiledVegetable.Cost.ToString();
                        caloriesTextBox.Text = boiledVegetable.Calories.ToString();
                        weightTextBox.Text = boiledVegetable.Weight.ToString();
                        countOfSeedsTextBox.Text = boiledVegetable.CountSeeds.ToString();
                        timeBoiledTextBox.Text = boiledVegetable.TimeBoiled.ToString();
                    } break;
                case "FreshVegetable":
                    {
                        FreshVegetable freshVegetable = (FreshVegetable)Ingredient;
                        nameTextBox.Text = freshVegetable.Name;
                        costTextBox.Text = freshVegetable.Cost.ToString();
                        caloriesTextBox.Text = freshVegetable.Calories.ToString();
                        weightTextBox.Text = freshVegetable.Weight.ToString();
                        countOfSeedsTextBox.Text = freshVegetable.CountSeeds.ToString();
                        timeCollectionTextBox.Text = freshVegetable.TimeCollection.ToString();
                    } break;
                case "Drupaceous":
                    {
                        Drupaceous drupaceous = (Drupaceous)Ingredient;
                        nameTextBox.Text = drupaceous.Name;
                        costTextBox.Text = drupaceous.Cost.ToString();
                        caloriesTextBox.Text = drupaceous.Calories.ToString();
                        weightTextBox.Text = drupaceous.Weight.ToString();
                        countryTextBox.Text = drupaceous.Country;
                        sweetTextBox.Text = drupaceous.Sweet;
                        sizeBoneTextBox.Text = drupaceous.SizeBone.ToString();
                    } break;
            }
        }
        
        public DialogResult Result { get; set; }
        private void okButton_Click(object sender, EventArgs e)
        {
            switch (typeComboBox.SelectedItem.ToString())
            {
                case "BoiledVegetable":
                    {
                       string name = nameTextBox.Text;
                       double cost = Convert.ToDouble(costTextBox);
                       double calories = Convert.ToDouble(caloriesTextBox.Text);
                       double weight = Convert.ToDouble(weightTextBox.Text);
                       int countSeeds = Convert.ToInt32(countOfSeedsTextBox.Text);
                       int timeBoiled = Convert.ToInt32(timeBoiledTextBox.Text);
                        Ingredient = new BoiledVegetable (name, cost, calories, weight,countSeeds,timeBoiled );
                        Result = DialogResult.OK;
                        this.Close();
                    } break;

                case "FreshVegetable":
                    {
                      
                       string name = nameTextBox.Text;
                       double cost = Convert.ToDouble(costTextBox);
                       double calories = Convert.ToDouble(caloriesTextBox.Text);
                       double weight = Convert.ToDouble(weightTextBox.Text);
                       int countSeeds = Convert.ToInt32(countOfSeedsTextBox.Text);
                       int timeCollection = Convert.ToInt32(timeCollectionTextBox.Text);
                        Ingredient = new FreshVegetable (name, cost, calories, weight, countSeeds, timeCollection);
                        Result = DialogResult.OK;
                        this.Close();
                    } break;

                case "Citrus":
                    {
                        string name = nameTextBox.Text;
                        double cost = Convert.ToDouble(costTextBox.Text);
                        double calories = Convert.ToDouble(caloriesTextBox.Text);
                        double weight = Convert.ToDouble(weightTextBox.Text);
                        int levelVitaminC = Convert.ToInt32(levelVitaminCTextBox.Text);
                        string country = countryTextBox.Text;
                        string sweet = sweetTextBox.Text;

                        Ingredient = new Citrus(name, cost, calories, weight, levelVitaminC, country, sweet);
                        Result = DialogResult.OK;
                        this.Close();
                    } break;
                case "Drupaceous":
                    {
                        string name = nameTextBox.Text;
                        double cost = Convert.ToDouble(costTextBox.Text);
                        double calories = Convert.ToDouble(caloriesTextBox.Text);
                        double weight = Convert.ToDouble(weightTextBox.Text);
                        double sizeBone = Convert.ToDouble(sizeBoneTextBox.Text);
                        string country = countryTextBox.Text;
                        string sweet = sweetTextBox.Text;

                        Ingredient = new Drupaceous(name, cost, calories, weight, sizeBone, country, sweet);
                        Result = DialogResult.OK;
                        this.Close();
                    } break;
            }
            
        }
        void EnableAllFields() 
        {
            countryTextBox.Enabled = true;
            timeCollectionTextBox.Enabled = true;
            levelVitaminCTextBox.Enabled = true;
            sizeBoneTextBox.Enabled = true;
            sweetTextBox.Enabled = true;
            timeBoiledTextBox.Enabled = true;
            countOfSeedsTextBox.Enabled = true;
        }
        void DisableFields(string type)
        {
            EnableAllFields();
      
            switch (type)
            {
                case "BoiledVegetable":
                    {
                        countryTextBox.Enabled = false;
                        timeCollectionTextBox.Enabled = false;
                        levelVitaminCTextBox.Enabled = false;
                        sizeBoneTextBox.Enabled = false;
                        sweetTextBox.Enabled = false;

                    } break;
                case "FreshVegetable":
                    {
                        countryTextBox.Enabled = false;
                        timeBoiledTextBox.Enabled = false;
                        levelVitaminCTextBox.Enabled = false;
                        sizeBoneTextBox.Enabled = false;
                        sweetTextBox.Enabled = false;

                    }break;
                case "Citrus":
                    {
                        countOfSeedsTextBox.Enabled = false;
                        sizeBoneTextBox.Enabled = false;
                        timeCollectionTextBox.Enabled = false;
                        timeBoiledTextBox.Enabled = false;

                    }break;
                case "Drupaceous":
                    {
                        countOfSeedsTextBox.Enabled = false;
                        levelVitaminCTextBox.Enabled = false;
                        timeCollectionTextBox.Enabled = false;
                        timeBoiledTextBox.Enabled = false;

                    }break;
                    
            }
           
        }
        private void typeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisableFields(typeComboBox.SelectedItem.ToString());
            
        }

        private void editButton_Click(object sender, EventArgs e)
        {

            switch (_type)
            {
                case "BoiledVegetable":
                    {
                        BoiledVegetable boiledVegetable = (BoiledVegetable)Ingredient;
                        boiledVegetable.Name = nameTextBox.Text;
                        boiledVegetable.Cost = Convert.ToDouble(costTextBox);
                        boiledVegetable.Calories = Convert.ToDouble(caloriesTextBox.Text);
                        boiledVegetable.Weight = Convert.ToDouble(weightTextBox.Text);
                        boiledVegetable.CountSeeds = Convert.ToInt32(countOfSeedsTextBox.Text);
                        boiledVegetable.TimeBoiled = Convert.ToInt32(timeBoiledTextBox.Text);
                        
                    } break;
                case "FreshVegetable":
                    {
                        FreshVegetable freshVegetable = (FreshVegetable)Ingredient;
                        freshVegetable.Name = nameTextBox.Text;
                        freshVegetable.Cost = Convert.ToDouble(costTextBox);
                        freshVegetable.Calories = Convert.ToDouble(caloriesTextBox.Text);
                        freshVegetable.Weight = Convert.ToDouble(weightTextBox.Text);
                        freshVegetable.CountSeeds = Convert.ToInt32(countOfSeedsTextBox.Text);
                        freshVegetable.TimeCollection = Convert.ToInt32(timeCollectionTextBox.Text);
                        
                    } break;
                case "Citrus":
                    {
                        Citrus citrus = (Citrus)Ingredient;
                        citrus.Name = nameTextBox.Text;
                        citrus.Cost = Convert.ToDouble(costTextBox.Text);
                        citrus.Calories = Convert.ToDouble(caloriesTextBox.Text);
                        citrus.Weight = Convert.ToDouble(weightTextBox.Text);
                        citrus.LevelVitaminC = Convert.ToInt32(levelVitaminCTextBox.Text);
                        citrus.Country = countryTextBox.Text;
                        citrus.Sweet = sweetTextBox.Text;

                    } break;
                case "Drupaceous":
                    {
                        Drupaceous drupaceous = (Drupaceous)Ingredient;
                        drupaceous.Name = nameTextBox.Text;
                        drupaceous.Cost = Convert.ToDouble(costTextBox.Text);
                        drupaceous.Calories = Convert.ToDouble(caloriesTextBox.Text);
                        drupaceous.Weight = Convert.ToDouble(weightTextBox.Text);
                        drupaceous.SizeBone = Convert.ToDouble(sizeBoneTextBox.Text);
                        drupaceous.Country = countryTextBox.Text;
                        drupaceous.Sweet = sweetTextBox.Text;

                    } break;
                    
            }
            this.Close();
        }

        private void costTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8 && e.KeyChar != ',') 
            e.Handled = true;
        }

        private void caloriesTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8 && e.KeyChar != ',')
                e.Handled = true;
        }

        private void weightTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8 && e.KeyChar != ',')
                e.Handled = true;
        }

        private void countOfSeedsTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8 && e.KeyChar != ',')
                e.Handled = true;
        }

        private void timeCollectionTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8 && e.KeyChar != ',')
                e.Handled = true;
        }

        private void timeBoiledTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8 && e.KeyChar != ',')
                e.Handled = true;
        }

        private void levelVitaminCTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8 && e.KeyChar != ',')
                e.Handled = true;
        }

        private void sizeBoneTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8 && e.KeyChar != ',')
                e.Handled = true;
        }
    }
}
