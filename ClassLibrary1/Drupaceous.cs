﻿
namespace Entities 
{
    public class Drupaceous : Fruit
    {
        private double _sizeBone;
        public Drupaceous(string name, double cost, double calories, double weight, double sizeBone, string country, string sweet) 
            : base ( name, cost, calories, weight, country, sweet)
        {
            this._sizeBone = sizeBone;
        }
        public double SizeBone
        {
            get { return _sizeBone; }
            set { _sizeBone = value; }
        }
    }
}
