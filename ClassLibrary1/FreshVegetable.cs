﻿
namespace Entities
{
    public class FreshVegetable : Vegetable
    {
        private int _timeCollection;
        public FreshVegetable(string name, double cost, double calories, double weight, int countSeeds, int timeCollection) 
            : base (name, cost, calories, weight, countSeeds )
        {
            this._timeCollection = timeCollection;
        }
        public int TimeCollection
        {
            get { return _timeCollection; }
            set { _timeCollection = value; }
        }
    }
}
