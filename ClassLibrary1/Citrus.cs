﻿
namespace Entities
{
    public class Citrus : Fruit
    {
        private int _levelVitaminC;

        public Citrus(string name, double cost, double calories, double weight, int levelVitaminC, string country, string sweet) 
            : base (name, cost, calories, weight, country, sweet)
        {
            this._levelVitaminC = levelVitaminC;
        }
        public int LevelVitaminC
        {
            get { return _levelVitaminC; }
            set { _levelVitaminC = value; }
        }
    }
}
