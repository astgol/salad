﻿
using System.Collections.Generic;

namespace Entities
{
    public class Salad
    {
        private List<AbstractIngredient> _ingredientList = new List<AbstractIngredient>();
        public void AddIngredient(AbstractIngredient Ingredient)
            {
                _ingredientList.Add(Ingredient);
            }
            public void DeleteIngredient(AbstractIngredient Ingredient)
            {
                _ingredientList.Remove(Ingredient);
            }
            public List<AbstractIngredient> Ingredient
            {
                get { return _ingredientList; }
            }
    }
}
