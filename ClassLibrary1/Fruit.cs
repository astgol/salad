﻿
namespace Entities
{
    public class Fruit : AbstractIngredient
    {
        private string _country;
        private string _sweet;

        public Fruit(string name, double cost, double calories, double weight,string country, string sweet) 
            : base (name,cost, calories, weight)
        {
            this._country = country;
            this._sweet = sweet;
        }
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public string Sweet
        {
            get { return _sweet; }
            set { _sweet = value; }
        }
    }
}
