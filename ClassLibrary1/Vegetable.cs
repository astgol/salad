﻿
namespace Entities
{
   public class Vegetable : AbstractIngredient
    {
        private int _countSeeds;
        public Vegetable(string name, double cost, double calories, double weight, int countSeeds) 
            : base (name, cost, calories, weight)
        {
            this._countSeeds = countSeeds;
        }
        public int CountSeeds
        {
            get { return _countSeeds; }
            set { _countSeeds = value; }
        }
    }
}
