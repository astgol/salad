﻿
namespace Entities
{
   abstract public class AbstractIngredient
    {
        private string _name;
        private double _cost;
        private double _calories;
        private double _weight;

       protected AbstractIngredient(string name, double cost, double calories, double weight)
        {
            this._name = name;
            this._cost = cost;
            this._calories = calories;
            this._weight = weight;
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public double Cost
        {
            get { return _cost; }
            set { 
                _cost = value;
                
            }
        }
        public double Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        public double Calories
        {
            get { return _calories; }
            set { _calories = value; }
        }
    }
}
