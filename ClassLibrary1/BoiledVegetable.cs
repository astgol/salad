﻿
namespace Entities
{
    public class BoiledVegetable : Vegetable
    {
        private int _timeBoiled;
        public BoiledVegetable(string name, double cost, double calories, double weight, int countSeeds, int timeBoiled) 
            : base(name, cost, calories, weight, countSeeds)
        {
            this._timeBoiled = timeBoiled;
        }
        public int TimeBoiled
        {
            get { return _timeBoiled; }
            set { _timeBoiled = value; }
        }
    }
}
