﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace Logic
{
    public class IngredientFactory
    {
        private static Salad salad = new Salad();
        public static Salad CreateSalad()
        {
            Citrus ananas = new Citrus("Ананас", 500, 300, 1 , 58, "Бразилия", "Кислый");
            salad.AddIngredient(ananas);

            BoiledVegetable potato = new BoiledVegetable("Картошка", 100, 100, 1, 0, 30);
            salad.AddIngredient(potato);

            Drupaceous apricot = new Drupaceous("Абрикос", 250, 200, 1, 30.5, "Грузия", "Сладкий");
            salad.AddIngredient(apricot);

            FreshVegetable beets = new FreshVegetable("Свекла", 80, 70, 1, 0, 40);
            salad.AddIngredient(beets);
         
           return salad;
        }
    }
}
