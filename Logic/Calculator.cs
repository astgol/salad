﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace Logic
{
    public class Calculator
    {
        public static double GetTotalCalories(Salad salad)
        {
         double totalCalories = 0;
         List<AbstractIngredient> ingredients = salad.Ingredient;

         foreach (AbstractIngredient ingredient in ingredients)
         {
             totalCalories += ingredient.Calories;
         }

        return totalCalories;
        }

        public static double GetTotalWeight(Salad salad)
        {
            double totalWeight = 0;
            List<AbstractIngredient> ingredients = salad.Ingredient;

            foreach (AbstractIngredient ingredient in ingredients)
            {
                totalWeight += ingredient.Weight;
            }

            return totalWeight;
        }

    }
    }

