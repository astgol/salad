﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic;
using Entities;

namespace SaladConsole
{
    public class IngredientPrinter
    {
        public static void Print(AbstractIngredient ingredient)
        {
            Console.WriteLine("Имя = " + ingredient.Name);
            Console.WriteLine("Калории = " + ingredient.Calories);
            Console.WriteLine("Вес = " + ingredient.Weight);
        }

        public static void PrintCalories(Salad salad)//СИГНАТУРА! void PrintCalories(Salad salad)
        {
            double calories;
            calories = Calculator.GetTotalCalories(salad);
            Console.WriteLine("Общие калории {0}", calories);
        }

        public static void PrintWeight(Salad salad)
        {
            double weight;
            weight = Calculator.GetTotalWeight(salad);
            Console.WriteLine("Общий вес {0}", weight);
        }
    }
}
