﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic;
using Entities;

namespace SaladConsole
{
    public class Program
    {
        static void Main(string[] args)
        {
            Salad salad = IngredientFactory.CreateSalad();
            var ingredients = salad.Ingredient;
           foreach (AbstractIngredient ingredient in ingredients)
           {
            IngredientPrinter.Print(ingredient);
           }
            IngredientPrinter.PrintCalories(salad);
            IngredientPrinter.PrintWeight(salad);
                Console.ReadKey();
        }
    }
}
